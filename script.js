


/*
	Operators

		- Assignment
		- Arithmetic
		- Compound Assignment

		- Comparison
		- Logical



*/


/* Assignment Operator ( = )

	- used to assign value to a variable


	Ex. 

	let a = 14;
	console.log(a);


	a = 7
	console.log(a);


	let b = a
	console.log(a, b);



*/



/* Arithmetic Operator




*/

	// addition operator
	// console.log(20 + 10);


	// subtraction operator
	// console.log(20 - 10);


	// multiplication operator
	// console.log(8 * 3);


	// division operator 
	// console.log(100 / 3);


	// modulo operator - (yung remainder ng quotient)
	// console.log(100 % 3);

	

/* Increment (++) & Decrement (--)*/



// let c = 30;

// prefix
// console.log(++c);
// console.log(c); 



// suffix

// console.log(5 + 5);

// let a = 5
// let b = 5

// console.log( a * b);


// console.log( 2 * (100 - 5))





/* Compound Assignment */
	
	- perform arithmetic operation -> then assigning back the value to the variable



		// Addition Assignment Operator (+=)


	let f = 15
/*  console.log (f += 3); //18
	console.log (f += 3); //21
	console.log (f += 3); //24*/





		// Subtraction Assignment Operator (-=)



	console.log (f -= 2);
	console.log (f -= 2);
	console.log (f -= 5);





































































